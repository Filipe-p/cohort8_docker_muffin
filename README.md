# Docker Compose - CATDOG Cake Store

In this repo we'll containarize a simple python app to serve an API with cakes and another container that is a php app that consumes the python api.


### Learning outcomes

- Git branching
- Git etiquete
- Good documentation (for devs and for infrastructure engineers)
- Docker & docker compose


Also touch on:

- Python
- APIs
- Webapps
- Seperation of concerns
- Frameworks vs Language

### Plan to complete

1. Create simple API with python - Put in container
2. Create simple PHP app to parse/consume JSON - put this in a container
3. Build docker compose file



### Python API 

We'll use Flask mini framework to create a simple API


### Docker compose

1. first:


```
# Docker-compose is a declarative way of building contains from images. 
# Imagine the docker run -d --name some_name -p 8000:3899 -e OTHER_VAR=VARSSSS some_image but in a yaml file. 

#usage to fire up 2 docker containers and make the speak easilly

version "3.0"

#well need to setup 2 services
services:
  product-api:
  web-server-php:

```

2. second: 


```
services:
  product-api:
    build: ./backend_product_api
    ports: 
      - 4000:80
    volumes:
      - ./backend_product_api:/usr/src/app
      # for development allows for live changes.
      # bomes a syched folder & dynamic to changes

```

3. Building a service directly from online image

```

  web-server-php:
    image: php:apache
    volumes:
      - ./frontend_php_app:/var/www/html
    ports:
      - 5000:80
    depends_on:
      - product-api
    
```


### Main commands

```

$ docker compose up -d

$ docker compose down 
```