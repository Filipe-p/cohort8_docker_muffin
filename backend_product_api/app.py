from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)
# print(type(app))
# print(type(api))

## Code for Flask API
## get all product names
class Product(Resource):
    def get(self):
        return {
            "products": ["CatDog Orange Muffin", "CatDog Blueberry Muffin", "CatDog Chocolate Muffin", "CatDog triple Chocolate Muffin" ]
        }

## Get all productPrice 
class ProductPrice(Resource):
    def get(self):
        return {
            "products": { "CatDog Orange Muffin": 10, "CatDog Blueberry Muffin": 6, "CatDog Chocolate Muffin": 6
            }
        } 

# To expose the api - send the Class(Product) and then give it a route("/")
api.add_resource(Product, "/")
api.add_resource(ProductPrice, "/price")


if __name__ == '__main__':
    print('name DID qual main!')
    app.run('0.0.0.0', port=80, debug=True)

















### Code for simple helloword in flask

# Mini framework that creates a running server
# use it for mini webapps or APIs
# if a mini server, you can recive get and post calls

# @app.route("/") # route on browser
# def hello_world(): # whar to return under said route
#     return "<p> Hello Fellow Flask User</p>"
#     # Go to DB and get data
#     # parse data into html file
#     # Return full html page


# @app.route("/about")
# def new_page():
#     return "<p> call us at: 0900739393</p>"